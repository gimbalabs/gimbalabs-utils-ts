import { Action, Data, UTxO } from '@meshsdk/core';

export interface RedeemValue {
  value: UTxO;
  script: UTxO;
  datum: UTxO;
  redeemer: Partial<Action>;
}

interface DatumProp {
  value: Data;
  inline: boolean;
}

interface RecipientProp {
  address: string;
  datum?: DatumProp;
}

export interface SendValue {
  recipient: RecipientProp;
  value: Partial<UTxO>;
}
