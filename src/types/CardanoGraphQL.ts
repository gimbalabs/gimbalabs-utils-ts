export declare type GraphQLUTxOResponse = {
    utxos: GraphQLUTxO[];
  };
  
  export declare type GraphQLTxResponse = {
    transactions: GraphQLTransaction[];
  };
  
  export declare type GraphQLUTxO = {
    txHash: string;
    index: number;
    address: string;
    value: string;
    tokens: GraphQLToken[];
    datum?: GraphQLDatum;
    script?: GraphQLReferenceScript;
  };
  
  export declare type GraphQLInputUTxO = {
    address: string;
    sourceTxHash?: string;
    sourceTxIndex?: number;
    value: string;
    tokens: GraphQLToken[];
  };
  
  export declare type GraphQLToken = {
    asset: {
      policyId: string;
      assetName: string;
      assetId: string;
      fingerprint?: string;
    };
    quantity: string;
  };
  
  export type GraphQLDatum = {
    bytes: string;
    value: {
      fields: any[];
      constructor: number;
    };
  };
  
  type GraphQLReferenceScript = {
    type: string;
    hash: string;
  };
  
  type GraphQLTransactionMetadata = {
    key: string;
    value: any;
  };
  
  export declare type GraphQLTransaction = {
    hash?: string;
    includedAt?: string;
    inputs: GraphQLInputUTxO[];
    outputs: GraphQLUTxO[];
    metadata: GraphQLTransactionMetadata[];
  };
  
  export type GraphQLProject = {
    constructor: 0;
    fields: [
      { bytes: string },
      { int: number },
      { int: number },
      { int: number },
    ];
  };
  