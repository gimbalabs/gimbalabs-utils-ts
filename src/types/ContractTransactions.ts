import { Recipient, UTxO, Asset, Action } from '@meshsdk/core';

export type ValueFromContract = {
  value: UTxO;
  script: UTxO;
  datum: UTxO;
  redeemer: Partial<Action>;
};

export type ValueToContract = {
  recipient: Recipient;
  value: Partial<UTxO>;
};

export type AssetsToContract = {
  recipient: Recipient;
  assets: Asset[];
};
