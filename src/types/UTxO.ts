import { UTxO } from '@meshsdk/core';
import { GraphQLUTxO } from './CardanoGraphQL';

export interface ResponseUTxOsList {
  graphQLUTxOs: GraphQLUTxO[];
  meshUTxOs: UTxO[];
}
export interface ResponseUTxO {
  graphQLUTxO: GraphQLUTxO;
  meshUTxO: UTxO;
}